/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

function sumNumber(){
	let firstNum = 5;
	let secondNum = 15;
	let addition = firstNum + secondNum;
	console.log("Displayed sum of 5 and 15");
	console.log(addition);
}
sumNumber();

function diffNumber(){
	let firstNum = 20;
	let secondNum = 5;
	let subtraction = firstNum - secondNum;
	console.log("Displayed difference of 20 and 5");
	console.log(subtraction);
}
diffNumber();

function productNumber(){
	let firstNum = 50;
	let secondNum = 10;
	let multiply = firstNum * secondNum;
	console.log("The product of 50 and 10:");
	console.log(multiply);
}
productNumber();

function quotientNumber(){
	let firstNum = 50;
	let secondNum = 10;
	let division = firstNum / secondNum;
	console.log("The quotient of 50 and 10:");
	console.log(division);
}
quotientNumber();

function areaNumber(radius){
	let pieNum = 3.1416;
	areaCircle = pieNum * radius ** 2;
	console.log("The result of getting the area of a circle with 15:");
	console.log(areaCircle);
}
areaNumber(15)

function averageVar(){
	let firstGrade = 20;
	let secondGrade = 40;
	let thirdGrade = 60;
	let fourthgrade = 80;
	averageNumber = (firstGrade + secondGrade + thirdGrade + fourthgrade) / 4;
	console.log("The average of 20,40,60 and 80:");
	console.log(averageNumber);
}
averageVar();

function myScore(score, total){
	return (score/total) * 100 > 75;
}

let isPassingScore = myScore(38, 50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);
